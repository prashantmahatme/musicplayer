package com.mvvm.musicplayerapp.ui.adapters

import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners

object BindAdapter {

    @JvmStatic
    @BindingAdapter(value = ["setImageUrl"])
    fun AppCompatImageView.bindImageUrl(url: String) {
        if (url.isNotBlank()) {
            Glide.with(this.context)
                .load(url)
                .transform(CenterCrop(), RoundedCorners(24))
                .into(this)
        }
    }
}