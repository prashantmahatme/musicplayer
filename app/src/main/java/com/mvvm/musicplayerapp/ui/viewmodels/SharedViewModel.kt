package com.mvvm.musicplayerapp.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bumptech.glide.Glide.init
import com.mvvm.musicplayerapp.data.database.entities.SongEntity
import com.mvvm.musicplayerapp.data.repository.ProjectRepository
import com.mvvm.musicplayerapp.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class SharedViewModel @Inject constructor(private val repository: ProjectRepository) : ViewModel() {

    var songslist: LiveData<List<SongEntity>> = repository.getAllSongs()
    var selectedSongEntity: MutableLiveData<SongEntity> = MutableLiveData()


    fun checkDbEmpty() = runBlocking {
        withContext(this.coroutineContext) {repository.checkDbEmpty()}
    }


    fun getPlaylistFromInternet(): MutableLiveData<Resource<List<SongEntity>>> {
        val response: MutableLiveData<Resource<List<SongEntity>>> = MutableLiveData()

        viewModelScope.launch {
            val songsResponse = repository.getSongsDb()
            response.postValue(Resource.Loading(null))

            if (songsResponse.isSuccessful) {
                response.postValue(Resource.Success(null))
            } else {
                response.postValue(Resource.Error("Something went wrong!"))
            }
        }
        return response;
    }

}

