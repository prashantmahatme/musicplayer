package com.mvvm.musicplayerapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.mvvm.musicplayerapp.R
import com.mvvm.musicplayerapp.data.database.entities.SongEntity
import com.mvvm.musicplayerapp.databinding.FragmentSongsListBinding
import com.mvvm.musicplayerapp.ui.adapters.PlaylistAdapter
import com.mvvm.musicplayerapp.ui.viewmodels.SharedViewModel
import com.mvvm.musicplayerapp.utils.Resource

class SongsListFragment : Fragment() {

    private lateinit var binding: FragmentSongsListBinding
    private val viewModel by activityViewModels<SharedViewModel>()
    private lateinit var adapter: PlaylistAdapter
    private var playlist: ArrayList<SongEntity> = arrayListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentSongsListBinding.inflate(inflater)
        initRecyclerView()
        observerData()
        return binding.root
    }

    private fun initRecyclerView() {
        // Set Adapter
        adapter = PlaylistAdapter(playlist)
        adapter.setItemClickListener {
            viewModel.selectedSongEntity.postValue(it)
            goToPlayerFrag()
        }
        binding.playlistRecyclerView.setHasFixedSize(true)
        val dividerItemDecoration =
            DividerItemDecoration(requireContext(), LinearLayoutManager.VERTICAL)
        binding.playlistRecyclerView.addItemDecoration(dividerItemDecoration)
        binding.playlistRecyclerView.adapter = adapter
    }

    private fun goToPlayerFrag() {
        activity
            ?.supportFragmentManager
            ?.beginTransaction()
            ?.replace(R.id.fragment_placeholder, PlayerFragment(), "player_frag")
            ?.addToBackStack(null)?.commit()
    }


    private fun observerData() {

        var isDbEmpty = viewModel.checkDbEmpty()
        if (!isDbEmpty) {
            viewModel.getPlaylistFromInternet().observe(viewLifecycleOwner, Observer {

                when (it) {
                    is Resource.Loading -> {
                        binding.progressBar.visibility = View.VISIBLE
                    }
                    is Resource.Success -> {
                        binding.progressBar.visibility = View.GONE
                    }
                    is Resource.Error -> {
                        binding.progressBar.visibility = View.GONE

                    }
                }

            })
        }
        viewModel?.songslist.observe(viewLifecycleOwner, Observer { songs ->
            print("observed data" + songs.size)

            songs?.let {
                playlist.clear()
                playlist.addAll(it)
                adapter.notifyDataSetChanged()
            }

        });

    }


}