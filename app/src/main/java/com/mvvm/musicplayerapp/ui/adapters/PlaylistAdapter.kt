package com.mvvm.musicplayerapp.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.mvvm.musicplayerapp.R
import com.mvvm.musicplayerapp.data.database.entities.SongEntity
import com.mvvm.musicplayerapp.databinding.PlaylistItemRowBinding
import com.mvvm.musicplayerapp.ui.adapters.PlaylistAdapter.ViewHolder

class PlaylistAdapter(val list: ArrayList<SongEntity>) : RecyclerView.Adapter<ViewHolder>() {

    private var itemClickListener: ((SongEntity) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        var binding: PlaylistItemRowBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.playlist_item_row,
                parent,
                false
            )

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.mItemView.song = list.get(position)
        holder.itemView.setOnClickListener { itemClickListener?.invoke(list.get(position)) }

    }

    override fun getItemCount(): Int = list?.size ?: 0

    inner class ViewHolder(val mItemView: PlaylistItemRowBinding) :
        RecyclerView.ViewHolder(mItemView.root) {
    }

    fun setItemClickListener(listener: (item: SongEntity) -> Unit) {
        itemClickListener = listener
    }

}