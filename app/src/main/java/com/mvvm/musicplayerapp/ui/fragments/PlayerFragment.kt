package com.mvvm.musicplayerapp.ui.fragments

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.mvvm.musicplayerapp.R
import com.mvvm.musicplayerapp.databinding.FragmentPlayerBinding
import com.mvvm.musicplayerapp.ui.viewmodels.SharedViewModel
import com.mvvm.musicplayerapp.utils.DataUtils.fromSecondsToMmSs
import com.mvvm.musicplayerapp.utils.PlayToPause
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class PlayerFragment : Fragment(), Player.EventListener {

    private lateinit var binding: FragmentPlayerBinding
    private val viewModel by activityViewModels<SharedViewModel>()

    private var currentPosition: Long = -1L
    private var prevSeekPosition: Long = -1L
    private var player: SimpleExoPlayer? = null

    private val dataSourceFactory by lazy {
        DefaultDataSourceFactory(requireContext(), "exoplayer-sample")
    }

    private val selectedSongEntity by lazy {
        viewModel.selectedSongEntity.value
    }

    var job: Job? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPlayerBinding.inflate(inflater)

        setup()
        // Inflate the layout for this fragment
        return binding.root
    }

    private fun setup() {
        selectedSongEntity?.let {
            binding.song = it
            binding.seekposition.text = "00:00"
        }

        binding.close.setOnClickListener {
            activity?.onBackPressed()
        }

        binding.play.setOnClickListener {
            binding.playView.findViewById<ImageButton>(R.id.exo_play).performClick()
            toggleController(PlayToPause.PlayToPause)
        }

        binding.pause.setOnClickListener {
            binding.playView.findViewById<ImageButton>(R.id.exo_pause).performClick()
            toggleController(PlayToPause.PauseToPlay)
        }

    }

    private fun initializePlayer() {
        player?.let {
            it.playWhenReady = true
            it.addListener(this)
            it.setMediaSource(buildMediaSource(binding.song?.audioLink))
            it.prepare()
            binding.playView.player = it
            binding.playView.requestFocus()

        } ?: let {
            player = SimpleExoPlayer.Builder(requireContext()).build()
            initializePlayer()
        }
    }

    override fun onResume() {
        super.onResume()
        initializePlayer()
    }


    override fun onPause() {
        super.onPause()
        stopProgressListener()
        releasePlayer()
    }

    private fun releasePlayer() {
        player?.let {
            it.removeListener(this)
            it.release()
            player = null
            toggleController(PlayToPause.PauseToPlay)
        }
    }

    private fun buildMediaSource(link: String?): MediaSource {
        return ProgressiveMediaSource.Factory(dataSourceFactory)
            .createMediaSource(MediaItem.fromUri(Uri.parse(link)))
    }

    override fun onPlaybackStateChanged(state: Int) {
        super.onPlaybackStateChanged(state)
        when (state) {
            Player.STATE_READY -> {
                seekToPosition()
                showControllerOnReady()
                CalculateDuration()
            }
            Player.STATE_ENDED -> {
                toggleController(PlayToPause.PauseToPlay)
            }
            Player.STATE_IDLE, Player.STATE_BUFFERING -> {
            }
        }
    }

    private fun showControllerOnReady() {
        toggleController(PlayToPause.PlayToPause)
    }

    private fun CalculateDuration() {
        player?.let {
            binding.totalDuration.text = fromSecondsToMmSs(it.duration)
            binding.seekposition.text = fromSecondsToMmSs(it.currentPosition)
            startProgressListener(it)
        }
    }

    private fun seekToPosition() {
        player?.let {
            if (currentPosition != -1L && (currentPosition != prevSeekPosition)) {
                prevSeekPosition = currentPosition
                it.seekTo(currentPosition)
            }
        }

    }


    private fun toggleController(pausePlay: PlayToPause) {
        when (pausePlay) {
            PlayToPause.PlayToPause -> {
                binding.play.visibility = View.GONE
                binding.pause.visibility = View.VISIBLE
            }
            PlayToPause.PauseToPlay -> {
                binding.pause.visibility = View.GONE
                binding.play.visibility = View.VISIBLE
            }
        }
    }

    private fun startProgressListener(player: SimpleExoPlayer) {
        if (job != null)
            return

        job = viewLifecycleOwner.lifecycleScope.launch {
            while (true) {
                val currentPosition = player.currentPosition
                //distinct untill changed
                if (this@PlayerFragment.currentPosition != currentPosition) {
                    this@PlayerFragment.currentPosition = currentPosition
                }
                binding.seekposition.text = fromSecondsToMmSs(currentPosition)

                //primary progress
                val progressed: Int = ((currentPosition * 100) / player.duration).toInt()
                binding.progressPlaceholder.progress = progressed

                //buffered progress
                val buffered: Int = ((player.bufferedPosition * 100) / player.duration).toInt()
                binding.progressPlaceholder.secondaryProgress = buffered

                delay(1000)
            }
        }
    }


    private fun stopProgressListener() {
        job?.cancel()
        job = null
    }


}