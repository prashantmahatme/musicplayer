package com.mvvm.musicplayerapp.ui

import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.mvvm.musicplayerapp.R
import com.mvvm.musicplayerapp.databinding.ActivityMainBinding
import com.mvvm.musicplayerapp.ui.fragments.SongsListFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        loadInitalFagment()
    }

    private fun loadInitalFagment() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_placeholder, SongsListFragment(), "playlist_fragment")
            .commit()

    }
}