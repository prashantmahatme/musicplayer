package com.mvvm.musicplayerapp.data.model

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Namespace
import org.simpleframework.xml.Root


@Root(name = "entry", strict = false)
data class Songs(

    @field:Element(name = "id")
    @param:Element(name = "id")
    var id: String = "",

    @field:Element(name = "title")
    @param:Element(name = "title")
    var title: String = "",

    @field:Element(name = "name")
    @param:Element(name = "name")
    var name: String = "",

    @field:ElementList(name = "link", required = false, inline = true)
    @param:ElementList(name = "link", required = false, inline = true)
    var link: List<Link> = arrayListOf(),


    @param:ElementList(name = "image", inline = true, required = false)
    @field:ElementList(name = "image", inline = true, required = false)
    @Namespace(prefix = "im")
    var images: List<Image> = ArrayList(),

    @field:Element(name = "releaseDate", required = false)
    @param:Element(name = "releaseDate", required = false)
    @Namespace(prefix = "im")
    var releaseDate: ReleaseDate? = null,


    @field:Element(name = "artist")
    @param:Element(name = "artist")
    @Namespace(prefix = "im")
    var artist: String = ""

)