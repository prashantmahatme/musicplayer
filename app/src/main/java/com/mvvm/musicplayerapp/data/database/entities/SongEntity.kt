package com.mvvm.musicplayerapp.data.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class SongEntity(
    @PrimaryKey val id: String,
    val title: String,
    val name: String,
    val artistName: String,
    val releaseDate: String,
    val audioLink: String,
    val duration: Long,
    val imageSmall: String,
    val imaageLarge: String
)
