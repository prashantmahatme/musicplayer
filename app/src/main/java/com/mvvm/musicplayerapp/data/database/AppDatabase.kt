package com.mvvm.musicplayerapp.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.mvvm.musicplayerapp.data.database.entities.SongEntity

@Database(entities = arrayOf(SongEntity::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun songsDao(): SongsDao
}