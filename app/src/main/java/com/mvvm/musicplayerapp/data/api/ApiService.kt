package com.mvvm.musicplayerapp.data.api

import com.mvvm.musicplayerapp.data.model.SongsResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET("WebObjects/MZStoreServices.woa/ws/RSS/topsongs/{limit}/xml")
    suspend fun getSongs(@Path("limit") limit: Int = 30):Response<SongsResponse>


}