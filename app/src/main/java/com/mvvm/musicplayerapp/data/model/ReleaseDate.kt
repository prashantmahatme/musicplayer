package com.mvvm.musicplayerapp.data.model

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Root
import org.simpleframework.xml.Text

@Root(strict = false)
data class ReleaseDate(

    @field:Attribute(name = "label", required = false)
    @param:Attribute(name = "label", required = false)
    var label: String? = null,

    @field:Text(required = false)
    @param:Text(required = false)
    var text: String? = null

)
