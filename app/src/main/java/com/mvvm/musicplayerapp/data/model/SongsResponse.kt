package com.mvvm.musicplayerapp.data.model

import org.simpleframework.xml.*
import retrofit2.http.Field


@Root(name = "feed", strict = false)
@NamespaceList(
    value = [
        Namespace(prefix = "im", reference = "http://itunes.apple.com/rss"),
    ]
)
data class SongsResponse @JvmOverloads constructor(

    @field:Element(name = "title")
    @param:Element(name = "title")
    var title: String = "",

    @field:ElementList(name = "entry", inline = true, required = false)
    @param:ElementList(name = "entry", inline = true, required = false)
    var songList: List<Songs> = ArrayList()

)
