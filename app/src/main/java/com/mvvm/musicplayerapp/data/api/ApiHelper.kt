package com.mvvm.musicplayerapp.data.api

import com.mvvm.musicplayerapp.data.model.SongsResponse
import retrofit2.Response

interface ApiHelper {

    suspend fun getSongs(): Response<SongsResponse>
}