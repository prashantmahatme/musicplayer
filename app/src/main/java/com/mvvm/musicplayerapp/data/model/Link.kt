package com.mvvm.musicplayerapp.data.model

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Namespace
import org.simpleframework.xml.Root

@Root(name = "link", strict = false)
class Link(
    @field:Attribute(name = "type", required = false)
    @param:Attribute(name = "type", required = false)
    var type: String? = null,

    @field:Attribute(name = "href", required = false)
    @param:Attribute(name = "href", required = false)
    var href: String? = null,

    @field:Element(name = "duration", required = false)
    @param:Element(name = "duration", required = false)
    @Namespace(prefix = "im")
    var duration: Long = 0

)