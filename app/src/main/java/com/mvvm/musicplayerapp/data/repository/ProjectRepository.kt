package com.mvvm.musicplayerapp.data.repository

import com.mvvm.musicplayerapp.data.api.ApiHelper
import com.mvvm.musicplayerapp.data.database.AppDatabase
import com.mvvm.musicplayerapp.data.database.entities.SongEntity
import com.mvvm.musicplayerapp.data.model.SongsResponse
import com.mvvm.musicplayerapp.utils.DataUtils
import retrofit2.Response
import javax.inject.Inject

class ProjectRepository @Inject constructor(
    private val apiHelper: ApiHelper,
    private val appDatabase: AppDatabase

) {

    suspend fun getOnlineSongs() = apiHelper.getSongs()

    suspend fun getSongsDb(): Response<SongsResponse> {
        val songsResponse = getOnlineSongs();
        songsResponse.body()?.let {
            val songs = DataUtils.convertToEntity(it);
            insertAll(songs);
        }
        return songsResponse
    }

    suspend fun insertAll(list: List<SongEntity>) {
        appDatabase.songsDao().insertAll(list)
    }

    fun getAllSongs() = appDatabase.songsDao().getSongs()

    suspend fun checkDbEmpty(): Boolean = appDatabase.songsDao().hasItem()
}