package com.mvvm.musicplayerapp.data.database

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mvvm.musicplayerapp.data.database.entities.SongEntity

@Dao
interface SongsDao {

    @Query("select * from songentity")
    fun getSongs(): LiveData<List<SongEntity>>

    @Query("SELECT EXISTS(SELECT * FROM songentity limit 1)")
    suspend fun hasItem(): Boolean

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(list: List<SongEntity>)
}