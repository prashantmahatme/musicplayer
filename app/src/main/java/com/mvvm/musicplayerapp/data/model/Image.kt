package com.mvvm.musicplayerapp.data.model

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root
import org.simpleframework.xml.Text

@Root(name = "image", strict = false)
data class Image(

    @field:Attribute(name = "height")
    @param:Attribute(name = "height")
    var height: Int = 0,

    @field:Text( required = false)
    @param:Text( required = false)
    var text: String = ""

)