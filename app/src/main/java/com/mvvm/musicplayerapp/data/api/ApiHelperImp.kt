package com.mvvm.musicplayerapp.data.api

import com.mvvm.musicplayerapp.data.model.SongsResponse
import retrofit2.Response
import javax.inject.Inject


class ApiHelperImp @Inject constructor(private val apiService: ApiService) : ApiHelper {

    override suspend fun getSongs(): Response<SongsResponse> = apiService.getSongs()

}