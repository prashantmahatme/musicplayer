package com.mvvm.musicplayerapp.utils

enum class PlayToPause {
    PlayToPause, PauseToPlay
}