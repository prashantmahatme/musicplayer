package com.mvvm.musicplayerapp.utils

import androidx.lifecycle.MutableLiveData


//sealed class Resource<T>
//data class Success<T>(val value: T) : Resource<T>()
//data class Failure<T>(val errorMessage: String) : Resource<T>()
//object Loading : Resource<Nothing>()

//sealed class Resource<T>() {
//    data class success<T>(val data: T?) : Resource<T>()
//    data class error<T>(val message: String?) : Resource<T>()
//    data class loading<T>() : Resource<T>()
//
//
//}

sealed class Resource<out T> {
    data class Loading(val data: String?) : Resource<Nothing>()
    data class Success<T>(val data: T?) : Resource<T>()
    data class Error(val error: String) : Resource<Nothing>()
}