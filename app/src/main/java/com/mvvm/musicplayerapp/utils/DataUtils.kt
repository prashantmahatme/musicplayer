package com.mvvm.musicplayerapp.utils

import com.mvvm.musicplayerapp.data.database.entities.SongEntity
import com.mvvm.musicplayerapp.data.model.Link
import com.mvvm.musicplayerapp.data.model.SongsResponse

object DataUtils {

    fun convertToEntity(songsResponse: SongsResponse): List<SongEntity> {
        val songList: ArrayList<SongEntity> = ArrayList<SongEntity>()

        songsResponse?.songList.map {

            val songItem = SongEntity(
                id = it.id,
                title = it.title,
                name = it.name,
                artistName = it.artist,
                releaseDate = it.releaseDate?.label ?: "",
                audioLink = it.link.get(it.link.indexOfFirst {  link ->  link.type == "audio/x-m4a" }).href.toString() ?: "",
                duration = it.link.get(it.link.indexOfFirst {  link ->  link.type == "audio/x-m4a" }).duration ?: 0,
                imageSmall = it.images.get(it.images.indexOfFirst { image -> image.height == 60 }).text ?: "",
                imaageLarge = it.images.get(it.images.indexOfFirst { image -> image.height == 170 }).text ?: ""
            )
            songList.add(songItem)
        }
        return songList
    }

    fun fromSecondsToMmSs(milliSec: Long): String {
        val s = (milliSec / 1000) % 60
        val m = ((milliSec / 1000) / 60) % 60
        return String.format("%02d:%02d", m, s)
    }

}